<?php require_once("./code.php")?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02: Repittion Control Structures and Array Manipulation</title>
</head>
<body>
    <h1>Repittion Control Structures</h1>

    <h2>While Loop</h2>
        <p><?php whileLoop() ?></p>
    <h2>Do While Loop</h2>
        <p><?php doWhileLoop() ?></p>
    <h2>For Loop</h2>
    <p><?php forLoop() ?></p>

    <h2>Modified For Loop</h2>
    <p><?php modifiedForLoop() ?></p>

    <h1>Array Manipulation</h1>

    <h2>Types of Array</h2>
   
    <h3>Simple Array</h3>
    <!--
        Syntax:
            foreach($array as $value/element){
               // code to be executed.
            }
     -->
        <ul>
            <?php foreach($computerBrands as $brand) { ?>
             <li><?php  echo $brand; ?></li>
            <?php } ?>
        </ul>

    <h3>Associative Array</h3>
        <ul>
                <?php foreach($gradePeriods as $period => $grade) { ?>
                    <li>Grade in <?php echo $period?> is <?php echo $grade?></li>
                    <?php } ?>
        </ul>

    <h3>Multidimensional Array</h3>        
    <!--pre><?php echo print_r($heroes); ?></pre-->
     <?php 

        foreach($heroes as $team){
            foreach($team as $member){
                
           
    ?> <li><?php echo $member;?></li>
    <?php } 
        }?>
 
 
   <h3>Multidimensional Associative Array</h3>

    <?php 
        foreach($ironManPowers as $label => $powerGroup){
            foreach($powerGroup as $power){

    ?>
        <li><?php echo "$label: $power"; ?></li>
     <?php
            }
        }
    ?>

    <h2>Array Functions</h2>
    <h3>Original Array</h3>

    <pre><?php print_r($computerBrands)?></pre>

    <h3>Sorting</h3>
    <pre><?php print_r($sortedBrands)?></pre>

    <h3>Reverse Sorting</h3>
    <pre><?php print_r($reverseSortedBrands)?></pre>

    <h3>Append</h3>
    <?php array_push($computerBrands, 'Apple')?>
    <pre><?php print_r($computerBrands) ?></pre>

    <?php array_unshift($computerBrands, 'Dell');?>
    <pre><?php print_r($computerBrands) ?></pre>

    <h3>Remove</h3>
    <?php array_pop($computerBrands)?>
    <pre><?php print_r($computerBrands) ?></pre>

    <?php array_shift($computerBrands);?>
    <pre><?php print_r($computerBrands) ?></pre>

    <h3>Others</h3>

    <h4>Count</h4>
        <pre><?php echo count($computerBrands);?></pre>

    <h4>In array</h4>
    <p><?php echo searchBrand($computerBrands, 'HP')?></p>
    <p><?php echo searchBrand($computerBrands, 'Acer')?></p>

    <h4>Reverse (not on descending order)</h4>
    <pre><?php print_r($reversedGradePeriods); ?></pre>
    <pre><?php print_r($gradePeriodsCopy); ?></pre>
</body>
</html>