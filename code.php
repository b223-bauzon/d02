<?php 

// [SECTION] Repitition Control Structures

//While loop

function whileLoop(){
    $count = 5;

    while($count !== 0){

        echo $count . '<br/>';
        $count --;
    }
}

//Do While Loop 

    function doWhileLoop(){
        $count = 20;

        do{

            echo $count .'<br/>';
            $count --;
        }while($count > 0);
    }

//For Loop
function forLoop(){
    
    for($count = 0; $count <=10; $count++){

        echo $count . '<br/>';

    };

}  

//Continue and Break
    function modifiedForLoop(){
        for($count = 0; $count <=20; $count++){
            if($count % 2 === 0){
                continue;
            }
            echo $count . '<br/>';
            if($count > 10){
                break;
            }
        }

    }

 // [SECTION] Array Manipulation
 
 //old php declaring array before php 5.4
 $studentNumbers = array('2020-1982','2020-2322','2020-2223');
 //$studentNumbers =  ['2020-1982','2020-2322','2020-2223'];

 //Simple Array
 $grades = array(98.5, 94.3, 89.2, 90.1);
 $computerBrands = array('Acer','Asus','Lenovo','Neo','Toshiba');

 //Associative Array
 $gradePeriods = [  
'firstGrading'  => 98.5,
 'secondGrading' => 94.3,
 'thirdGrading'  => 89.5,
 'fourthGrading' => 90.5,];

 $heroes = [
    ['ironman','thor','hulk'],
    ['wolverine','cyclops','jean gray'],
    ['batman','superman','wonderwoman']
 ];

 //Two dimensional associative array
 $ironManPowers = [
    'regular' => ['replusor blast', 'rocket punch'],
    'signature' => ['unibeam']

 ];

 //Array Sorting
 $sortedBrands = $computerBrands;
 $reverseSortedBrands = $computerBrands;

 //Ascending Order
 sort($sortedBrands);
 //Descending Order
 rsort($reverseSortedBrands);

//Search FUnction

function searchBrand($brands, $brand){
 // in_array(#searchVal, $arrayLust)

    return(in_array($brand, $brands) ? "$brand is in the array." : "$brand is not in the array." );
}


$reversedGradePeriods = array_reverse($gradePeriods);
$gradePeriodsCopy = $gradePeriods;
asort($gradePeriodsCopy);

?>